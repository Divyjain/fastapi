from fastapi import FastAPI
import pymysql
import json
from pydantic import BaseModel
import flask
from flask import request, jsonify
from datetime import date, datetime

app = FastAPI()

conn = pymysql.connect(
host='localhost',
user='root',
port= 3306,
password = "divy@datavizz",
db='test')

class create_dict(dict):
    def __init__(self):
        self = dict()
        

    def add(self, key, value):
        self[key] = value


@app.get("/api/g")

async def root():

    mydict=create_dict()

    cur = conn.cursor()
    result=cur.execute("select * from dob")
    print(result)
    output = cur.fetchall()
    
    for row in output:
        mydict.add(row[0],({"Name":row[0],"BirthDate":row[1]}))
    
    employee_data = json.dumps(mydict, indent=None, sort_keys=True, default=str)
    file = employee_data.replace('\n', '')    
    data = json.loads(file)
    print(data)
    return data     


class Item(BaseModel):
    Name: str 
    BirthDate: str
     
@app.post('/api/p')
   
async def root1(item: Item):
    mydict=create_dict()

    cur = conn.cursor()
    q = "INSERT INTO dob VALUES (%s,%s)"
    cur.execute(q,(item.Name,item.BirthDate))
    conn.commit()
    print(q)
    
    
    result=cur.execute("select * from dob")
    print(result)
    output = cur.fetchall()
    
    for row in output:
        mydict.add(row[0],({"Name":row[0],"BirthDate":row[1]}))
    
    employee_data = json.dumps(mydict, indent=None, sort_keys=True, default=str)
    file = employee_data.replace('\n', '')    
    data = json.loads(file)
    print(data)
    return data 



@app.put('/api/put')
async def root2(item: Item):
    mydict=create_dict()

    cur = conn.cursor()
    q = "update dob set name =%s where dob = %s ;"
    cur.execute(q,(item.Name,item.BirthDate))
    conn.commit()
    print(q)
    
    
    result=cur.execute("select * from dob")
    print(result)
    output = cur.fetchall()
    
    for row in output:
        mydict.add(row[0],({"Name":row[0],"BirthDate":row[1]}))
    
    employee_data = json.dumps(mydict, indent=None, sort_keys=True, default=str)
    file = employee_data.replace('\n', '')    
    data = json.loads(file)
    print(data)
    return data     


@app.delete('/api/delete')   
async def root3(item: Item):
    mydict=create_dict()

    cur = conn.cursor()
    q = "DELETE FROM dob WHERE name = %s; "
    cur.execute(q,(item.Name))
    conn.commit()
    print(q)
    
    
    result=cur.execute("select * from dob")
    print(result)
    output = cur.fetchall()
    
    for row in output:
        mydict.add(row[0],({"Name":row[0],"BirthDate":row[1]}))
    
    employee_data = json.dumps(mydict, indent=None, sort_keys=True, default=str)
    file = employee_data.replace('\n', '')    
    data = json.loads(file)
    print(data)
    return data     